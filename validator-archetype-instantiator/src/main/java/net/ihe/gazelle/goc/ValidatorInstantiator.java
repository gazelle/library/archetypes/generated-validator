package net.ihe.gazelle.goc;

public class ValidatorInstantiator {

    private static final String CD_COMMAND = "cd \"";

    public static String generateArchetype(String workspacePath, String mvnExecPath, String gocVersion, String projectName, String projectNameCapitalyzed,
                                           String configuration, String projectNameCapitalyzedFirst, String projectServiceName, String folderOutput,
                                           String ignoreCdaBasic) {
        String loadMethod;
        String validatorRootClass;
        if ("cdaepsos".equals(configuration)) {
            validatorRootClass = "net.ihe.gazelle.cdaepsos3.POCDMT000040ClinicalDocument";
            loadMethod="load";
        } else {
            validatorRootClass = "net.ihe.gazelle.cda.POCDMT000040ClinicalDocument";
            loadMethod = "loadBasic";
        }
        return CD_COMMAND + workspacePath + "\"\n" + mvnExecPath + " archetype:generate -B " +
                "-DarchetypeGroupId=net.ihe.gazelle.goc " +
                "-DarchetypeArtifactId=validator-archetype " +
                "-DarchetypeVersion=" + getVersion() + " " +
                "-DprojectName=" + projectName + " " +
                "-DgroupId=net.ihe.gazelle.goc " +
                "-DartifactId=" + projectName + "-validator-jar " +
                "-Dversion=" + gocVersion + " " +
                "-DprojectNameCapitalyzed=" + projectNameCapitalyzed + " " +
                "-DvalidatorRootClass=" + validatorRootClass + " " +
                "-DloadMethod=" + loadMethod + " " +
                "-DprojectNameCapitalyzedFirst=" + projectNameCapitalyzedFirst + " " +
                "-DignoreCdaBasic=" + ignoreCdaBasic + " " +
                "-DfolderOutput=\"" + folderOutput + "\" " +
                "-DprojectServiceName=\"" + projectServiceName + "\"";
    }

    private static String getVersion(){
        return ValidatorInstantiator.class.getPackage().getImplementationVersion();
    }
}
