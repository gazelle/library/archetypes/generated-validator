#!/bin/bash

echo "from generateAndUpdate: {$1} {$2}"

mvn mbgen:genvalidator

mvn exec:exec@UpdateImports -DcdaProps="${1}" -DprojectName="${2}"