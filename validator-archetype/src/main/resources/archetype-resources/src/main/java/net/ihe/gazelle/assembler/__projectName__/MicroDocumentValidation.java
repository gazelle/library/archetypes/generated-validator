package net.ihe.gazelle.assembler.${projectName};

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.ValidationEvent;
import javax.xml.bind.ValidationEventLocator;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import net.ihe.gazelle.cdaepsos3.cdaepsos3.CDAEPSOS3PackValidator;
import net.ihe.gazelle.cda.cdabasic.CDABASICPackValidator;
import net.ihe.gazelle.cda.cdadt.CDADTPackValidator;
import net.ihe.gazelle.cda.cdanblock.CDANBLOCKPackValidator;
import net.ihe.gazelle.${projectName}.${projectName}.${projectNameCapitalyzed}PackValidator;
import net.ihe.gazelle.${projectName}arb.${projectNameCapitalyzedFirst}Parser;
import net.ihe.gazelle.com.templates.Template;
import net.ihe.gazelle.gen.common.CommonOperations;
import net.ihe.gazelle.gen.common.ConstraintValidatorModule;
import net.ihe.gazelle.gen.common.DatatypesNamespaceContext;
import net.ihe.gazelle.gen.common.TemplateParser;
import net.ihe.gazelle.utils.ValueSetProviderClass;
import net.ihe.gazelle.utils.XMLValidation;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.DocumentValidXSD;
import net.ihe.gazelle.validation.DocumentWellFormed;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.MDAValidation;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validation.ValidationResultsOverview;
import net.ihe.gazelle.validation.XSDMessage;
import net.sf.saxon.om.NodeInfo;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

class MicroDocumentValidation {


	static {
		CommonOperations.setValueSetProvider(new ValueSetProviderClass());
	}

	private MicroDocumentValidation(){}

	public static void validateWithBasicCDAValidator(String documentContent, List<ConstraintValidatorModule> listConstraintValidatorModule,
			List<TemplateParser> listParser, DetailedResult res) throws FileNotFoundException{
		net.ihe.gazelle.cda.POCDMT000040ClinicalDocument clin = null;
		List<Notification> ln = new ArrayList<Notification>();
		try {
			clin = CDATemplateParser.loadBasic(new ByteArrayInputStream(documentContent.getBytes(StandardCharsets.UTF_8)));

			if (clin == null) throw new Exception();
			for (ConstraintValidatorModule constraintValidatorModule : listConstraintValidatorModule) {
				net.ihe.gazelle.cda.POCDMT000040ClinicalDocument.validateByModule(clin, "/ClinicalDocument", constraintValidatorModule, ln);
			}
			Template temp = CDATemplateParser.generateTemplateDescriberFromObject(clin, listParser, "/ClinicalDocument", ln);
			if (temp != null && !temp.getTemplate().isEmpty()) {
				res.setTemplateDesc(temp);
			}
		} catch (ValidatorException e) {
			errorWhenExtracting(e, ln);
		}
		catch (Exception e) {
			errorWhenExtracting(ln);
		}
		updateValidationResult(documentContent, res, ln);
	}

	public static void validateWithExtendedCDAValidator(String documentContent, List<ConstraintValidatorModule> listConstraintValidatorModule,
			List<TemplateParser> listParser, DetailedResult res) throws FileNotFoundException{
		${validatorRootClass} clin = null;
		List<Notification> ln = new ArrayList<Notification>();
		try {
			clin = CDATemplateParser.load(new ByteArrayInputStream(documentContent.getBytes(StandardCharsets.UTF_8)));
//			clin = loadCDA(new ByteArrayInputStream(req.getBytes("UTF8")));
			if (clin == null) throw new Exception();
			for (ConstraintValidatorModule constraintValidatorModule : listConstraintValidatorModule) {
				${validatorRootClass}.validateByModule(clin, "/ClinicalDocument", constraintValidatorModule, ln);
			}
			Template temp = CDATemplateParser.generateTemplateDescriberFromObject(clin, listParser, "/ClinicalDocument", ln);
			if (temp != null && !temp.getTemplate().isEmpty()) {
				res.setTemplateDesc(temp);
			}
		} catch (ValidatorException e) {
			errorWhenExtracting(e, ln);
		}
		catch (Exception e) {
			errorWhenExtracting(ln);
		}
		updateValidationResult(documentContent, res, ln);
	}

	private static DetailedResult updateValidationResult(String documentContent, DetailedResult res, List<Notification> ln) throws FileNotFoundException {
		if (res == null) {
			return null;
		}
		if (res.getMDAValidation() == null){
			res.setMDAValidation(new MDAValidation());
		}
		if (documentContent != null){
			Notification not = validateCDAIDs(documentContent);
			ln.add(not);
		}
		for (Notification notification : ln) {
			res.getMDAValidation().getWarningOrErrorOrNote().add(notification);
		}
		return res;
	}

	private static Notification validateCDAIDs(String documentContent){
		Notification not = null;
		List vals = evaluate(new ByteArrayInputStream(documentContent.getBytes(StandardCharsets.UTF_8)),
				" //cda:reference/@value", new DatatypesNamespaceContext());
		List ids = evaluate(new ByteArrayInputStream(documentContent.getBytes(StandardCharsets.UTF_8)),
				" //@ID", new DatatypesNamespaceContext());
		String badValues = "";
		List<String> listId = new ArrayList<String>();
		if (ids != null){
			for (int i = 0; i < ids.size(); i++) {
				if (ids.get(i) instanceof NodeInfo) {
					listId.add(((NodeInfo)ids.get(i)).getStringValue());
				}
			}
		}

		if (vals != null){
			for (int i = 0; i < vals.size(); i++) {
				if (vals.get(i) instanceof NodeInfo) {
					String val = ((NodeInfo)vals.get(i)).getStringValue();
					if(val.length()>1 && (val.charAt(0) == '#')){
						if (!listId.contains(val.substring(1))){
							if (!badValues.equals("")) badValues+=",";
							badValues += val.substring(1);
						}
					}
				}
			}
		}
		if (badValues.equals("")){
			not = new Note();
			not.setTest("test_IDs");
			not.setLocation("/ClinicalDocument");
			not.setDescription("Success: Found all IDs referenced");
		}
		else{
			not = new Error();
			not.setTest("test_IDs");
			not.setLocation("/ClinicalDocument");
			not.setDescription("Error: Cannot find all the IDs referenced. Missing IDs are : " + badValues );
		}
		return not;
	}


	public static List evaluate(InputStream stream, String expression,
			NamespaceContext namespace){
		List string = null;
		try{
			InputSource source = new InputSource(stream);

			XPathFactory fabrique = XPathFactory.newInstance();
			XPath xpath = fabrique.newXPath();
			if(namespace != null){
				xpath.setNamespaceContext(namespace);
			}

			XPathExpression exp = xpath.compile(expression);
			string = (List)exp.evaluate(source,XPathConstants.NODESET);

		}catch(Exception xpee){
			System.out.println("Error when trying to validate the references of IDs" + xpee.getClass().getSimpleName() + "," + xpee.getMessage());
		}
		return string;
	}

	public static DetailedResult validateUsingGeneratedGOC(String documentPath) throws FileNotFoundException {
		DetailedResult dr = new DetailedResult();
		List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
		String documentContent = flattenNullFlavor(documentPath, dr);

		listConstraintValidatorModule.add(new ${projectNameCapitalyzed}PackValidator());
		List<TemplateParser> ltp = new ArrayList<>();
		ltp.add(new ${projectNameCapitalyzedFirst}Parser());

		// --conditional validation
		listConstraintValidatorModule.add(new CDAEPSOS3PackValidator());
		MicroDocumentValidation.validateWithExtendedCDAValidator(documentContent, listConstraintValidatorModule, ltp, dr);

		${validatorRootClass} clin = null;
		String fullDocumentContent = "";
		try {
			clin =  CDATemplateParser.load(new FileInputStream(documentPath));
			fullDocumentContent = NullFlavorChecker.nodeToString(clin.get_xmlNodePresentation());
		} catch (Exception e) {
			errorWhenExtracting(new ArrayList<Notification>());
		}
        validateCDABasic(fullDocumentContent, dr, ${ignoreCdaBasic});

		validateToSchema(dr, documentPath);
		updateMDAValidation(dr);;
		summarizeDetailedResult(dr);
		return dr;
	}

	private static void validateCDABasic(String fullDocumentContent, DetailedResult dr, boolean ignoreCDABasic) throws FileNotFoundException {
	    if (!ignoreCDABasic){
			List<ConstraintValidatorModule> listConstraintValidatorModule = new ArrayList<>();
            // --
            listConstraintValidatorModule.add(new CDABASICPackValidator());
            listConstraintValidatorModule.add(new CDADTPackValidator());
            listConstraintValidatorModule.add(new CDANBLOCKPackValidator());
            MicroDocumentValidation.validateWithBasicCDAValidator(fullDocumentContent, listConstraintValidatorModule, null, dr);
        }
	}

	private static String flattenNullFlavor(String documentPath, DetailedResult res) throws FileNotFoundException{

        ${validatorRootClass} clin = null;
		List<Notification> ln = new ArrayList<Notification>();
		String documentContent = null;
		try {
			clin = CDATemplateParser.${loadMethod}(new FileInputStream(documentPath));

			Node clinNode = clin.get_xmlNodePresentation();
			NullFlavorChecker nullFlavorChecker = new NullFlavorChecker();
			documentContent = nullFlavorChecker.flatten(clinNode, "/ClinicalDocument", ln);
            ${validatorRootClass} clinNullFlavorChecked = CDATemplateParser.${loadMethod}(new ByteArrayInputStream(NullFlavorChecker.nodeToString(clinNode).getBytes()));
			if (clinNullFlavorChecked == null) throw new Exception();
			else
				documentContent = NullFlavorChecker.nodeToString(clinNode);
		} catch (ValidatorException e) {
			errorWhenExtracting(e, ln);
		}
		catch (Exception e) {
			errorWhenExtracting(ln);
		}
		updateValidationResult(documentContent, res, ln);
		return documentContent;
	}

	private static void updateMDAValidation(DetailedResult dr){
		if (dr != null && dr.getMDAValidation() != null) {
			dr.getMDAValidation().setResult("PASSED");
			for (Object notification : dr.getMDAValidation().getWarningOrErrorOrNote()) {
				if (notification instanceof net.ihe.gazelle.validation.Error){
					dr.getMDAValidation().setResult("FAILED");
				}
			}
		}
	}

	static void errorWhenExtracting(ValidatorException vexp, List<Notification> ln){
		if (ln ==null) return;
		if (vexp != null){
			if (vexp.getDiagnostic() != null){
				for (Notification notification : vexp.getDiagnostic()) {
					ln.add(notification);
				}
			}
		}
	}

	static void errorWhenExtracting(List<Notification> ln){
		if (ln ==null) return;
		Error err = new Error();
		err.setTest("structure");
		err.setLocation("All the document");
		err.setDescription("The tool is not able to find urn:hl7-org:v3:ClinicalDocument element as the root of the validated document.");
		ln.add(err);
	}

	static void summarizeDetailedResult(DetailedResult dr){
		if (dr != null){
			Date dd = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy, MM dd");
			DateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");
			dr.setValidationResultsOverview(new ValidationResultsOverview());
			dr.getValidationResultsOverview().setValidationDate(dateFormat.format(dd));
			dr.getValidationResultsOverview().setValidationTime(timeFormat.format(dd));
			dr.getValidationResultsOverview().setValidationServiceName("Gazelle CDA Validation : ${projectServiceName}");
			dr.getValidationResultsOverview().setValidationTestResult("PASSED");
			if ((dr.getDocumentValidXSD() != null) && (dr.getDocumentValidXSD().getResult() != null) && (dr.getDocumentValidXSD().getResult().equals("FAILED"))){
				dr.getValidationResultsOverview().setValidationTestResult("FAILED");
			}
			if ((dr.getDocumentWellFormed() != null) && (dr.getDocumentWellFormed().getResult() !=null) && (dr.getDocumentWellFormed().getResult().equals("FAILED"))){
				dr.getValidationResultsOverview().setValidationTestResult("FAILED");
			}
			if ((dr.getMDAValidation() != null) && (dr.getMDAValidation().getResult() != null) && (dr.getMDAValidation().getResult().equals("FAILED"))){
				dr.getValidationResultsOverview().setValidationTestResult("FAILED");
			}
		}
	}

	private static void validateToSchema(DetailedResult res, String documentPath){
		DocumentWellFormed dd = new DocumentWellFormed();
		dd = XMLValidation.isXMLWellFormed(documentPath);
		res.setDocumentWellFormed(dd);
		if (res.getDocumentWellFormed().getResult().equals("PASSED")){
			try{
				res.setDocumentValidXSD(XMLValidation.isXSDValid(documentPath));
			}
			catch(Exception e){
				System.out.println("error when validating with the schema : " + e.getMessage());
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				PrintStream ps = new PrintStream(baos);
				e.printStackTrace(ps);
				DocumentValidXSD docv = new DocumentValidXSD();
				docv.setResult("FAILED");
				docv.getXSDMessage().add(new XSDMessage());
				docv.getXSDMessage().get(0).setSeverity("error");
				docv.getXSDMessage().get(0).setMessage("error when validating with the schema. The error is : " + StringUtils.substring(baos.toString(), 0, 300));
				res.setDocumentValidXSD(docv);
			}
		}
	}

	protected static boolean handleEvent(ValidationEvent event, ValidatorException vexp) {
		 if (event.getSeverity() != ValidationEvent.WARNING) {
             ValidationEventLocator vel = event.getLocator();
             Notification not = new Error();
             not.setDescription("Line:Col[" + vel.getLineNumber() +
                     ":" + vel.getColumnNumber() +
                     "]:" +event.getMessage());
             not.setTest("message_parsing");
             not.setLocation("All the document");
             vexp.getDiagnostic().add(not);
         }
         return true;
	}

}
