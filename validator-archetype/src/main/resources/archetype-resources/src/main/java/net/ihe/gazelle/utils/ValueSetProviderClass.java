package net.ihe.gazelle.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.*;

import net.ihe.gazelle.gen.common.Concept;
import net.ihe.gazelle.gen.common.ValueSetProvider;
import net.ihe.gazelle.gen.common.XmlUtil;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ValueSetProviderClass implements ValueSetProvider{

	public static Map<String, List<Concept>> listConcept = new HashMap<String, List<Concept>>();

	@Override
	public List<Concept> getConceptsListFromValueSet(String valueSetId, String lang) {
		return getConceptsListFromValueSet(valueSetId, null, lang);
	}

	@Override
	public List<Concept> getConceptsListFromValueSet(String valueSetId, String version, String lang) {
		if (listConcept.get(valueSetId + version + lang) != null) {
			return listConcept.get(valueSetId + version + lang);
		}
		try {
			String filePath =  net.ihe.gazelle.utils.ProjectDependencies.VALUE_SET_REPOSITORY + File.separator + valueSetId;
			if (version != null) {
				filePath += "-" + version.replace(":", "");
				filePath += ".xml";
			} else {
				filePath += ".xml";
				if (!new File(filePath).exists()){
					File valueSetRepository = new File(net.ihe.gazelle.utils.ProjectDependencies.VALUE_SET_REPOSITORY);
					ArrayList<String> filesForValueSetOid = new ArrayList<>(Arrays.asList(valueSetRepository.list(new ValueSetByOidFilter(valueSetId))));
					Collections.sort(filesForValueSetOid);
					filePath = net.ihe.gazelle.utils.ProjectDependencies.VALUE_SET_REPOSITORY + File.separator + filesForValueSetOid.get(filesForValueSetOid.size()-1);
				}
			}

			String content = net.ihe.gazelle.utils.FileReadWrite.readDoc(filePath);
			Document document = XmlUtil.parse(content);
			NodeList concepts = document.getElementsByTagName("Concept");
			if (concepts.getLength() > 0)
			{
				List<Concept> conceptsList = new ArrayList<Concept>();
				for(int index = 0; index < concepts.getLength(); index ++)
				{
					Node conceptNode = concepts.item(index);
					NamedNodeMap attributes = conceptNode.getAttributes();
					Concept aa = new Concept();
					if (attributes.getNamedItem("code") != null) aa.setCode(attributes.getNamedItem("code").getTextContent());
					if (attributes.getNamedItem("displayName") != null) aa.setDisplayName(attributes.getNamedItem("displayName").getTextContent());
					if (attributes.getNamedItem("codeSystem") != null) aa.setCodeSystem(attributes.getNamedItem("codeSystem").getTextContent());
					if (attributes.getNamedItem("codeSystemName") != null) aa.setCodeSystemName(attributes.getNamedItem("codeSystemName").getTextContent());

					conceptsList.add(aa);
				}
				listConcept.put(valueSetId + version + lang, conceptsList);
				return conceptsList;
			}
			else
			{
				listConcept.put(valueSetId + version + lang, null);
				return null;
			}

		} catch (Exception e) {
			listConcept.put(valueSetId + version + lang, null);
			return null;
		}
	}


	class ValueSetByOidFilter implements FilenameFilter {
		private String ms_oid;

		public ValueSetByOidFilter(String ps_oid){
			this.ms_oid = ps_oid;
		}

		@Override
		//return true if find a file named "OID-EXTENSION.xml", extension being optional
		public boolean accept(final File pf_dir, final String ps_name) {
			return ps_name.startsWith(ms_oid + "-") && ps_name.endsWith(".xml");
		}
	}
}
